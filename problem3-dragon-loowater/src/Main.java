import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer[] diameters = {5,4};
        Integer[] heights = {7,8,4};
        totalHeight(convertArrayToList(diameters),convertArrayToList(heights));

        Integer[] diameters1 = {7, 2};
        Integer[] heights1 = {2, 1, 8, 5};
        totalHeight(convertArrayToList(diameters1),convertArrayToList(heights1));

        Integer[] diameters2 = {7, 2};
        Integer[] heights2 = {4, 3, 1, 2};
        totalHeight(convertArrayToList(diameters2),convertArrayToList(heights2));

        Integer[] diameters3= {5, 10};
        Integer[] heights3 = {5};
        totalHeight(convertArrayToList(diameters3),convertArrayToList(heights3));
    }
    
    public static void totalHeight(List<Integer> diameters, List<Integer>heights) {
        List<Integer> list = new ArrayList<>();
        Integer totalHeights = 0;

        if (diameters.size() > heights.size()) {
            System.out.println("knight fall");
            return;
        }

        for (int i = 0; i<diameters.size(); i++) {
            Integer[] array = {Integer.MAX_VALUE, -1};

            for (int j = 0; j < heights.size(); j++) {
                if (diameters.get(i) <= heights.get(j) && heights.get(j) - diameters.get(i) < array[0]) {
                    array[0] = heights.get(j) - diameters.get(i);
                    array[1] = heights.get(j);
                }
            }
            if (array[1] != -1) {
                totalHeights += array[1];
                list.add(array[1]);
            } else {
                break;
            }
        }

        if (list.size() > 0) {
            System.out.println(totalHeights);
        } else {
            System.out.println("knight fall");
        }
    }

    public static List<Integer> convertArrayToList(Integer[] array) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i<array.length; i++) {
            list.add(array[i]);
        }
        return list;
    }

}